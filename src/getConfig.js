function getConfig(jsxRoot, projectWebpackPath) {
  var config = require("../webpack.config.js");
  config.resolve.root = jsxRoot

  if (projectWebpackPath) {
    const projectWebpack = require(projectWebpackPath)
    
    // Support all extensions that the target app supports
    if (projectWebpack.resolve && projectWebpack.resolve.extensions) {
      config.resolve.extensions = projectWebpack.resolve.extensions
    }
    if (projectWebpack.module && projectWebpack.module.loaders) {
      const babelConfig = config.module.loaders[0]

      config.module.loaders = projectWebpack.module.loaders
        .map(loader => {
          if (
            (loader.loader && loader.loader === 'babel') ||
            (loader.loaders && loader.loaders.indexOf('babel') !== -1)
          ) {
            return babelConfig
          }
          return loader
        })
    }
  }

  return config
}

module.exports = getConfig
