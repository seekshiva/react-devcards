import React, { Component } from 'react'
import ProxyComponent from './ProxyComponent'

export default class MyComponent extends Component {
  render(){
    return (
      <div>
        <h4>Your devcard would appear here:</h4>
        <hr />
        <div>
          <ProxyComponent />
        </div>
       </div>
    )
  }
}
