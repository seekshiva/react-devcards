var path = require("path");
var webpack = require("webpack")

module.exports = {
  entry: {
    app: [
      'webpack-dev-server/client?http://localhost:8080',
      'webpack/hot/only-dev-server',
      "devcardRoot/app/main.js"
    ]
  },
  output: {
    path: path.resolve(__dirname, "build"),
    publicPath: "/assets/",
    filename: "bundle.js",
    sourceMapFilename: '[name].map'
  },
  // resolve.root will be injected
  resolve: {
    alias: {
      devcardRoot: __dirname
    },
    extensions: ['', '.js', '.jsx']
  },
  devServer: {
    headers: { "Access-Control-Allow-Origin": "*" },
    proxy: {
      '/static*': {
        target: 'https://localhost:5000/static',
        secure: false
      }
    },
  },
  devtool: '#inline-source-map',
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules\/((?!react-devcards).)|bower_components/,
        loaders: ['react-hot', 'babel']
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
};
