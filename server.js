var path = require('path')
var webpack = require("webpack")
var webpackDevServer = require('webpack-dev-server')
var exec = require('child_process').exec;
function puts(error, stdout, stderr) { console.log(stdout) }

function Run(config){
  new webpackDevServer(webpack(config), {
    contentBase: config.output.path,
    publicPath: config.output.publicPath,
    hot: true,
    historyApiFallback: true
  }).listen(8080)
}

module.exports.Run = Run
