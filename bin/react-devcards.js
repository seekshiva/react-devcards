#!/usr/bin/env node
'use strict';

const fs = require('fs')
const path = require('path')
const getConfig = require('../src/getConfig.js')
const walk = require('../src/walk.js')
const server = require('../server.js')

if (!process.argv[2]) {
  console.log('Usage: react-devcards <component-file-name>')
  process.exit(1)
}
var componentName = process.argv[2]

const devcardsPath = path.resolve(__dirname, '..')
const jsxRoot = path.resolve(devcardsPath, '..', '..', 'jsx')

console.log('\n\n', 'Search directory', jsxRoot, '\n\n')

function getPackageRoot() {
  const cwd = process.cwd()

  // Is the pwd somewhere within an npm package?
  let root = cwd
  while (!fs.existsSync(path.join(root, 'package.json'))) {
    if (root === '/') {
      root = cwd
      break
    }
    root = path.resolve(root, '..')
  }

  return root
}

function getWebpackPath() {
  const cwd = jsxRoot

  // Is the pwd somewhere within an npm package?
  let root = cwd
  while (!fs.existsSync(path.join(root, 'webpack.config.js'))) {
    if (root === '/') {
      return null
    }
    root = path.resolve(root, '..')
  }

  return path.join(root, 'webpack.config.js')
}

(() => {
  const cwdPackageRoot = getPackageRoot()
  const packageJSONPath = path.join(cwdPackageRoot, 'package.json');
  if (fs.existsSync(packageJSONPath)) {
    const packageJSON = require(packageJSONPath)
    if (!packageJSON['react-devcards']) {
      console.error(
        'Missing "react-devcards" section in your package.json. ' +
        'Check https://somesite for more info.'
      )
      process.exit()
    }
    else {
      const devcardsConfig = packageJSON['react-devcards']
      const jsIncludesConfig = devcardsConfig['js']
      const cssIncludesConfig = devcardsConfig['css']
      console.log(packageJSON['react-devcards'])
      let jsIncludes = ''
      let cssIncludes = ''

      if (jsIncludesConfig) {
        let jsIncludesArr = jsIncludesConfig
        if (typeof jsIncludesConfig === 'string') {
          jsIncludesArr = [jsIncludesConfig]
        }
        jsIncludes = jsIncludesArr
          .map(url => '<script type="text/javascript" src="' + url + '"></script>')
          .join('\n  ')
      }
      if (cssIncludesConfig) {
        let cssIncludesArr = cssIncludesConfig
        if (typeof cssIncludesConfig === 'string') {
          cssIncludesArr = [cssIncludesConfig]
        }
        cssIncludes = cssIncludesArr
          .map(url => '<link rel="stylesheet" type="text/css" href="' + url + '" />')
          .join('\n  ')
      }

      const htmlTemplatePath = path.join(devcardsPath, 'app', 'index.template.html')
      const htmlTemplate = fs.readFileSync(htmlTemplatePath, 'utf8')

      const finalHtml = htmlTemplate
        .replace('{{ CSS_INCLUDES }}', cssIncludes)
        .replace('{{ JS_INCLUDES }}', jsIncludes)

      writeIndexHtml(finalHtml)

      findMatchingFile(jsxRoot, componentName, filename => {
        console.log('filename', filename)
        writeProxyComponent(filename)

        const projectWebpackPath = getWebpackPath()
        const config = getConfig(jsxRoot, projectWebpackPath)
        
        server.Run(config)
      })
    }
  }
})()

function findMatchingFile(jsxRoot, query, callback) {
  const myPath = jsxRoot

  walk(myPath, query, function(err, results) {
    if (err) throw err;
    if (results.length === 1) {
      callback(results[0])
    }
    else {
      if(results.length === 0) {
        console.error('Unable to find file that matches query: ', query)
      }
      else {
        console.error(
          'Can you be more specific? which of these is it?\n' +
          results.map(result => ' - /jsx' + result.slice(jsxRoot.length) + '\n').join('')
        )
      }
      process.exit()
    }
  });
}

function writeIndexHtml(content) {
  const buildPath = path.join(devcardsPath, 'build')
  if (!fs.existsSync(buildPath)) {
    console.log('doesn\'t exist')
    fs.mkdirSync(buildPath)
  }
  const indexFilePath = path.join(buildPath, 'index.html')

  fs.writeFileSync(indexFilePath, content)
}

function writeProxyComponent(filename) {
  var proxyComponentPath = path.join(devcardsPath, 'app', 'ProxyComponent.js')
  var content = 'module.exports = require("' + filename + '")'

  fs.writeFileSync(proxyComponentPath, content)
}
